/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
	preset: 'ts-jest',
	testEnvironment: 'node',
	collectCoverageFrom: ['<rootDir>/src/**/*.ts'],
	coverageDirectory: 'coverage',
	coverageProvider: 'babel',
	moduleNameMapper: {
		'@/tests/(.+)': '<rootDir>/tests/$1',
		'@/(.+)': '<rootDir>/src/$1',
	},
	roots: ['<rootDir>/src', '<rootDir>/tests'],
	transform: {
		'\\.ts$': 'ts-jest',
	},
};
